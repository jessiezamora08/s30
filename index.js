// SETP 1 : create a folder called s30
// STEP 2: make a file called index.js 
// STEP 3: open your terminal to s30
// STEP 4: Login to MongoDB Atlas
// STEP 5: inside s30 npm init
// STEP 6: npm install express

// STEP 7:  import express: 
const express = require ('express')
const mongoose = require('mongoose') // STEP 13
// STEP 8: create express app:
const app = express();

//STEP 9: create a const called PORT
const port = 3001

// STEP 10: SETUP SERVER TO HANDLE DATA FROM REQUESTS, allows our 
// app to read JSON data 
app.use (express.json())

// allows our app to read ata from forms 
app.use (express.urlencoded({extended:true})) 

// STEP 11: add listen method for requests - DAPAT LAST ITO NA ENTRY
// app.listen(port, () => console.log (`server running at port ${port}`))

// step 12: install mongoose
//mongoose is a package that allowa creation of Schemas to model out data structures 
// put "npm install mongoose" to terminal to achieve this 

// STEP 13: import mongoose
// const mongoose = require('mongoose')

//STEP 14: go to mongoDB Atlas and change the network access to 0.0.0.0

// STEP 15 : get connection string and CHANGE PASSWORD:
// mongodb+srv://admin:admin1234@zuitt-bootcamp.c1f1f.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

// STEP 16: change myFirstDatabase  to s30. MOngoDB will automatically create
// the database for us 

// mongodb+srv://admin:admin1234@zuitt-bootcamp.c1f1f.mongodb.net/s30?retryWrites=true&w=majority

//STEP 17 Connecting to MongoDB Atlas - add connect()
//mongoose.connect();

//STEP 18a add the connection string
//STEP 18b add this object to allow connection

/*
{
    useNewUrlParser: true
    useUnifiedTopology: true
}
*/
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.c1f1f.mongodb.net/s30?retryWrites=true&w=majority',
{
    useNewUrlParser: true,
    useUnifiedTopology: true
}
);

//STEP 19 Set notification for connection success or failure by using .connection property 
//mongoose.connection; 

//STEP 20 store it in a variable call db 
let db = mongoose.connection; 

// STEP 21 - allows to print error in the browser and in the terminal 
db.on('error', console.error.bind(console, 'connection error'))

// STEP 22 is the connection is succesful, print this in the console
db.once("open",() => console.log ("We're connected to the cloud database!!!!"))


//STEP 23  : SCHEMAS determine the structure of the documents to be written in the database;
// Schemas act as blueprints to our data 
// use the SCHEMA() constructir if the Mongoose module to create a new Schema object
const taskSchema = new mongoose.Schema({
    // define the fields with the corresponding data type 
    // for a task, it needs a "task name" and "task status"
    // There is a field called "name" and its data tyoe is "String"
    name: String, 
    //there is a field called "status" that is a "String" and the default value is "pending"
    status: {
        type: String, 
        default: "pending"
    }
});

// models use Schemas and they act as middleman from the server (JS COde) to our database 
// models represents the collection. if your want to add, use this method

// STEP 24: CREATE A MODEL ; MODELS must be in singular form and capitalized 
// the first parameter if the Mongoose model method indicates the collection 
// in where to store the data (tasks, plural form if the model)

//The second parameter is used to specify the Schema blueprint 
// of the documents that will be stored in the MongoDB collection 

//Using mongoose, the package was programmed well enough that it automatically 
// converts the singular form of the model name into plural form when creating a collection in postman 

const Task = mongoose.model ("Task", taskSchema)

/*
Business Logic 
1. Add a funcitonality to check if there are duplicate tasks 
 - if the task already exists in the database, we return an error 
 - if the task doesn't exist in the database, add it in the database 
*/
//STEP 25: create route to add task 

// app.post('/tasks', (req,res)=> {
//     req.body.name; 
// })

// STEP 26: check if the task already exist. use the task model to interact with the tasks collection 
app.post('/tasks', (req,res)=> {
    Task.findOne({name: req.body.name}, (err,result) => {
        // if result of fineone is not equal to null (may laman) and nasa database
        if(result != null && result.name == req.body.name){
            // return a message to the cliet / postman  

           return res.send ('Duplicate task found!!!') 
        }else{
            let newTask = new Task({
                name: req.body.name
            })

            newTask.save((saveErr, savedTask) => {
                if(saveErr){
                    return console.error(saveErr)
                }else{
                    return res.status(201).send('New Task created')
                }
            })
        }
    })
})


// STEP 27 : Get all tasks 
//  see the contents of the task colection via the Task model
// "find" is a Mongoose method that is similar to MongoDb "find", and an empty "{}" means it returns 
// all the documents and stores them in the "result" parameter of the callback functions
app.get('/tasks', (req,res)=> {
    Task.find({}, (err,result) =>{
        if(err){
            return console.log(err)
        }else{
            return res.status(200).json({data:result})
        }
    })
})


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GUIDED ACTIVITY:


// 1. Create a User Schema

const userSchema = new mongoose.Schema({
    
    username: String, 
    password: String
});


// 2. Make a model 
const User = mongoose.model ("User", userSchema)

// 3. Register a User 

app.post('/users', (req,res)=> {
    User.findOne({username: req.body.username}, (err,result) => {
    
        if(result != null && result.username == req.body.username){
        

           return res.send ('This username already exist!') 
        }else{
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            })

            newUser.save((saveErr, savedUser) => {
                if(saveErr){
                    return console.error(saveErr)
                }else{
                    return res.status(201).send('New User Registered.')
                }
            })
        }
    })
})

//3a. ROUTE for creating a user for sign up 

app.post('/signup' , (req,res) => {

})



// do not erase

app.listen(port, () => console.log (`server running at port ${port}`))